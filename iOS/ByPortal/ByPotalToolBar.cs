#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ByPortal
{
	/// <summary>
	/// By potal tool bar.
	/// </summary>
	public partial class ByPotalToolBar : UIView
	{
		public static readonly UINib Nib = UINib.FromName ("ByPotalToolBar", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ByPotalToolBar");

		public ByPotalToolBar (IntPtr handle) : base (handle)
		{
		}

		public static ByPotalToolBar Create ()
		{
			return (ByPotalToolBar)Nib.Instantiate (null, null) [0];
		}

		public MonoTouch.UIKit.UIButton ButtonDate 
		{ 
			get {
				return (MonoTouch.UIKit.UIButton)this.ViewWithTag (200);
			}
		}

		public MonoTouch.UIKit.UIButton ButtonDistance 
		{ 
			get {
				return (MonoTouch.UIKit.UIButton)this.ViewWithTag (201);
			} 
		}
	}
}

