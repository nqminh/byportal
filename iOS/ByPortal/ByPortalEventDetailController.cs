#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MBProgressHUD;

namespace ByPortal
{
	/// <summary>
	/// By portal event detail controller.
	/// </summary>
	public partial class ByPortalEventDetailController : UIViewController
	{
		private static UIImage AlarmBackground = UIImage.FromBundle("alarm-active.png");
		private static UIImage FavoriteBackground = UIImage.FromBundle("favorite-active.png");
		private static UIImage FacebookBackground = UIImage.FromBundle("facebook-active.png");
		private static UIImage CheckinBackground = UIImage.FromBundle("checkin-active.png");

		private static UIImage IconFavoriteActive = UIImage.FromBundle("favorite-icon-active.png");
		private static UIImage IconFavoriteDeactive = UIImage.FromBundle("favorite-icon-deative.png");

		private MTMBProgressHUD hud;

		public Event eventInfo { get; set; }

		public ByPortalEventDetailController () : base ("ByPortalEventDetailController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public void Resize()
		{
			float offset_y = this.canvaDetails.Frame.Y;
			float hDesc = ByPotalUtils.HeightFromString (this.lblDescription.Text);
			int line = (int)(hDesc / 18);
			RectangleF newf = this.lblDescription.Frame;
			newf.Height = hDesc;
			this.lblDescription.Lines = line;
			this.lblDescription.Frame = newf;

			RectangleF newf1 = this.lblLocation.Frame;
			newf1.Y = newf.Y + newf.Height;
			this.lblLocation.Frame = newf1;

			UIScrollView scrolv = (UIScrollView)this.View;
			scrolv.ContentSize = new SizeF (320, offset_y + newf1.Y + newf1.Height + 5);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.Title = this.eventInfo.Teaser;		

			UIButton backButton = new UIButton (UIButtonType.Custom);
			string urlImage = this.eventInfo.DetailImage.Trim ();

			backButton.Frame = new RectangleF (0, 0, 53, 30);
			backButton.SetBackgroundImage(UIImage.FromBundle("button_back_active.png"),UIControlState.Normal);
			backButton.TouchUpInside += this.backAction;

			//urlImage +=  "&height=" + (2 * this.bannerImageView.Frame.Size.Height).ToString ();

			this.NavigationItem.LeftBarButtonItem = new UIBarButtonItem (backButton);
			this.indicator.Hidden = false;
			this.indicator.StartAnimating ();
			this.bannerImageView.setImageURL (urlImage,this.BannerImageLoaded);

			this.lblDateTime.Text = ByPotalUtils.DescriptionDuringTime1(this.eventInfo.StartTime, this.eventInfo.EndTime);
			this.lblHeadline.Text = this.eventInfo.Headline.Trim();
			this.lblDescription.Text = this.eventInfo.Description.Trim();
			this.lblLocation.Text = this.eventInfo.Address.Trim();
			this.btnFavorite.SetBackgroundImage((this.eventInfo.Favorite ? IconFavoriteActive : IconFavoriteDeactive),
			                                    UIControlState.Normal);

			this.Resize ();					
		}

		void BannerImageLoaded(UIImage loadedImage)
		{
			this.indicator.StopAnimating ();
			this.indicator.Hidden = true;

			if (loadedImage != null) {

				float h_0 = (loadedImage.Size.Height * 320) / loadedImage.Size.Width;

				if (h_0 > 350) {
					h_0 = 350;

					float h_i = (loadedImage.Size.Width * h_0) / 320.0f;
					loadedImage = ByPotalUtils.Crop (loadedImage, new RectangleF (0,loadedImage.Size.Height - 350,loadedImage.Size.Width,h_i));
					this.bannerImageView.Image = loadedImage;
				}

				RectangleF f_0 = this.bannerImageView.Frame;
				RectangleF f_1 = this.canvaActions.Frame;
				RectangleF f_2 = this.canvaDetails.Frame;

				f_0.Height = h_0;
				f_1.Y = f_0.Y + f_0.Height - 20;
				f_2.Y = f_1.Y + f_1.Height + 10;

				UIView.BeginAnimations ("BIL");
				UIView.SetAnimationDuration (0.2);
				this.bannerImageView.BackgroundColor = UIColor.Black;
				this.bannerImageView.Frame = f_0;
				this.canvaActions.Frame = f_1;
				this.canvaDetails.Frame = f_2;
				this.Resize ();
				UIView.CommitAnimations ();
			}
		}

		void openRounter()
		{
			if (ByPortalViewController.currentLocation != null)
			{
				string urlString = string.Format("http://maps.apple.com/maps?saddr={0},{1}&daddr={2},{3}",
				                                 ByPortalViewController.currentLocation.Coordinate.Latitude,
				                                 ByPortalViewController.currentLocation.Coordinate.Longitude,
				                                 this.eventInfo.Latitude,
				                                 this.eventInfo.Longitude);

				NSUrl url = new NSUrl (urlString);
				UIApplication.SharedApplication.OpenUrl (url);
			}
			else {
				new UIAlertView ("ByPortal", "Could't locate your position", null, "OK", null).Show();
			}
		}

		void HandleDidHide (object sender, EventArgs e)
		{
			hud.RemoveFromSuperview();
			hud = null;
		}

		void ShowHUD()
		{
			hud = new MTMBProgressHUD(this.NavigationController.View);
			this.NavigationController.View.AddSubview(hud);

			// Regiser for DidHide Event so we can remove it from the window at the right time
			hud.DidHide += HandleDidHide;

			// Add information to your HUD
			hud.LabelText = "ByPortal";
			hud.DetailsLabelText = "Posting...";
			hud.Square = true;
			hud.Show (true);

		}

		void DidFacebookPost (NSError error)
		{
			hud.Hide (true);
		}

		void DidFacebookLogin(NSError error)
		{
			if (error == null) {

				this.ShowHUD ();
				FacebookUtils.PostStatus (this.eventInfo,this.DidFacebookPost);
			}
			else {
				FacebookUtils.ResetSession ();
			}
		}

		void openFacebook()
		{
			if (FacebookUtils.IsAuthorize() == false)
				FacebookUtils.Login(this.DidFacebookLogin);			
			else {
				this.ShowHUD ();
				FacebookUtils.PostStatus (this.eventInfo,this.DidFacebookPost);
			}
		}

		void backAction(object sender, EventArgs e)
		{
			this.NavigationController.PopViewControllerAnimated (true);
		}

		void showAlarmView(bool show)
		{
			RectangleF f_0 = this.viewPopup.Frame;
			UIDatePicker dpk = (UIDatePicker)this.viewPopup.ViewWithTag (100);

			if (this.eventInfo.Alarm == null) {
				DateTime time = ByPotalUtils.ShortTimeToDate (this.eventInfo.StartTime);
				NSDate dt = time;//DateTime.SpecifyKind (time, DateTimeKind.Local);
				dpk.Date = dt;

			}
			else {
				NSDate dt = this.eventInfo.Alarm;// DateTime.SpecifyKind (this.eventInfo.Alarm, DateTimeKind.Local);
				dpk.Date = dt;
			}

			f_0.Y = (UIScreen.MainScreen.Bounds.Height == 568 ? 568 : 480);
			f_0.Height = (UIScreen.MainScreen.Bounds.Height == 568 ? 568 : 480);

			if (show) {
				this.viewPopup.Frame = f_0;
				this.NavigationController.View.AddSubview (this.viewPopup);

				f_0.Y = 0;

				UIView.BeginAnimations ("XV");
				UIView.SetAnimationDuration (0.2);
				this.viewPopup.Frame = f_0;
				UIView.CommitAnimations ();
			}
			else {
				this.viewPopup.RemoveFromSuperview ();
			}
		}

		partial void alarm (MonoTouch.Foundation.NSObject sender)
		{
			this.actionBackground.Image = AlarmBackground;
			this.showAlarmView(true);	
		}
			
		partial void favorite (MonoTouch.Foundation.NSObject sender)
		{
			this.actionBackground.Image = FavoriteBackground;
			this.eventInfo.UpdateFavorite(!this.eventInfo.Favorite);
			this.btnFavorite.SetBackgroundImage((this.eventInfo.Favorite ? IconFavoriteActive : IconFavoriteDeactive),
			                                    UIControlState.Normal);
		}

		partial void checkin (MonoTouch.Foundation.NSObject sender)
		{
			this.actionBackground.Image = CheckinBackground;
			this.openRounter();
		}

		partial void facebook (MonoTouch.Foundation.NSObject sender)
		{
			this.actionBackground.Image = FacebookBackground;
			this.openFacebook();

		}

		partial void cancelAlarm (MonoTouch.Foundation.NSObject sender)
		{
			this.showAlarmView(false);
		}

		void AddAlarmToNotification()
		{
			NSDate dt = this.eventInfo.Alarm; //DateTime.SpecifyKind (this.eventInfo.Alarm, DateTimeKind.Local);
			if (dt != null) {
				UILocalNotification notif = new UILocalNotification ();
				NSDictionary userInfo = NSDictionary.FromObjectAndKey (new NSString (this.eventInfo.Teaser),
				                                                       new NSString("kByPortalEventAlarm"));

				notif.FireDate = dt;
				notif.TimeZone = NSTimeZone.DefaultTimeZone;
				notif.AlertBody = this.eventInfo.Teaser;
				notif.AlertAction = "Show event";
				notif.RepeatInterval = 0;
				notif.UserInfo = userInfo;

				UIApplication.SharedApplication.ScheduleLocalNotification (notif);
			}
		}

		partial void doneAlarm (MonoTouch.Foundation.NSObject sender)
		{
			UIDatePicker dpk = (UIDatePicker)this.viewPopup.ViewWithTag (100);
			//DateTime dateAlarm = DateTime.SpecifyKind(dpk.Date, DateTimeKind.Unspecified);
			this.eventInfo.Alarm = dpk.Date;
			this.AddAlarmToNotification();
			this.showAlarmView(false);
		}
	}
}

