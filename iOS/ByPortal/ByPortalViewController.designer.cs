// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ByPortal
{
	[Register ("ByPortalViewController")]
	partial class ByPortalViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton ButtonDate { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton ButtonDistance { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView tableView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView toolbarView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ButtonDate != null) {
				ButtonDate.Dispose ();
				ButtonDate = null;
			}

			if (ButtonDistance != null) {
				ButtonDistance.Dispose ();
				ButtonDistance = null;
			}

			if (tableView != null) {
				tableView.Dispose ();
				tableView = null;
			}

			if (toolbarView != null) {
				toolbarView.Dispose ();
				toolbarView = null;
			}
		}
	}
}
