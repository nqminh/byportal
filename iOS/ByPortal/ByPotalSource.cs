#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using MonoTouch.CoreLocation;

namespace ByPortal
{
	public class ByPotalSource: UITableViewSource
	{
		private List<Event> events;
		private ByPortalViewController parentVC;

		public ByPotalSource (List<Event> newEvents,ByPortalViewController aParentVC)
		{
			this.events = newEvents;
			this.parentVC = aParentVC;
		}


		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			return null;
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{		
			return 2;
		}

		public override float GetHeightForFooter (UITableView tableView, int section)
		{
			return 2;
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return events.Count;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return 1;
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return 100.0f;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			ByPotalEventCell cell = ByPotalEventCell.Create ();
			Event theEvent = events [indexPath.Section];

			cell.Title.Text = theEvent.Teaser.Trim();
			cell.ShortDescription.Text = theEvent.Description.Trim();
			cell.Date.Text = ByPotalUtils.DescriptionDuringTime (theEvent.StartTime, theEvent.EndTime);
			cell.Photo.setImageURL (theEvent.Thumbnail,null);
			UIView backgroundInCell = new UIView (cell.Frame);
			backgroundInCell.BackgroundColor = UIColor.White;
			cell.BackgroundView = backgroundInCell;

			return cell;
		}
			
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			Event theEvent = events [indexPath.Section];
			ByPortalEventDetailController edc = new ByPortalEventDetailController ();
			edc.eventInfo = theEvent;
			this.parentVC.NavigationController.PushViewController (edc,true);
		}
	}
}

