using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Json;
using System.Collections.Generic;
using ByPortal;

namespace CityPortalCore
{
	public class WSController
	{
		private static WSController instant;

		private List<Event> listEvents = null;

		private WSController ()
		{
		}

		public static WSController getInstant(){
			if (instant == null) {
				instant = new WSController ();
			}
			return instant;
		}

		public Event findEventById(int id){
			foreach (Event e in listEvents) {
				if (e.ID == id) {
					return e;
				}
			}
			return null;
		}

		public List<Event> getListEventsFromMemory(){
			return listEvents;
		}

		public async Task<int> requestLoadListEvents(OnLoadListItem listener){
			var httpClient = new HttpClient ();
			Task<string> contentsTask = httpClient.GetStringAsync ("http://syddanskedage.dk/umbraco/api/eventsapi/events");
			// await! control returns to the caller and the task continues to run on another thread
			string contents = await contentsTask;
			//parse json to objects
			if (listEvents == null) {
				listEvents = new List<Event> ();
			} else {
				listEvents.Clear ();
			}
			var array = JsonArray.Parse (contents);
			foreach(JsonObject obj in array){
				var e=new Event();
				e.ID = obj ["Id"];
				e.Saturday = obj ["Saturday"];
				e.Sunday = obj ["Sunday"];
				e.StartTime = obj ["StartTime"];
				e.EndTime = obj ["EndTime"];
				e.Teaser = obj["Teaser"];
				e.Headline = obj ["Headline"];
				e.Description = obj ["Description"];
				e.Place = obj ["Place"];
				e.Address = obj ["Address"];
				e.Address2 = obj ["Address2"];
				e.Number = obj ["Number"];
				e.Zip = obj ["Zip"];
				e.City = obj ["City"];
				e.Latitude = obj ["Latitude"];
				e.Longitude = obj ["Longitude"];
				e.Thumbnail = obj ["Thumbnail"];
				e.DetailImage = obj ["DetailImage"];
				e.RegistrationRequired = obj ["RegistrationRequired"];
				e.PaymentRequired = obj ["PaymentRequired"];
				listEvents.Add(e);
			}
			listener.onLoadListItem (listEvents);
			return 1;
		}
	}
}

