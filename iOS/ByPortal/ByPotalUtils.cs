using MonoTouch.CoreImage;

#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Json;
using MonoTouch.CoreGraphics;

namespace ByPortal
{
	/// <summary>
	/// By potal utils.
	/// </summary>
	public class ByPotalUtils : NSObject
	{
		private static ByPotalUtils sharedUtils = new ByPotalUtils();
		private static string EventURL = "http://syddanskedage.dk/umbraco/api/eventsapi/events"; 

		public static void NetworkSpinStart()
		{
			if (NSThread.Current.IsMainThread) {
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			} 
			else {
				ByPotalUtils.sharedUtils.BeginInvokeOnMainThread (delegate {
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
				});
			}
		}

		public static void NetworkSpinStop()
		{
			if (NSThread.Current.IsMainThread) {
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			} 
			else {
				ByPotalUtils.sharedUtils.BeginInvokeOnMainThread (delegate {
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
				});
			}
		}

		public static float HeightFromString (string aString)
		{
			NSString aaString = new NSString (aString);
			SizeF fsize = new SizeF (300, 999);
			SizeF aSize = aaString.StringSize (UIFont.SystemFontOfSize (14),fsize);

			return aSize.Height;
		}

		public static string DatabaseFilePath {
			get { 
				var storeFilename = "EventDB.plist";
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				//string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
				var path = Path.Combine (documentsPath, storeFilename);
			
				return path;	
			}
		}

		public static List<Event> ListEventRequester()
		{
			NSUrl urlEvent = NSUrl.FromString (EventURL);
			NSMutableUrlRequest request = new NSMutableUrlRequest (urlEvent);
			NSUrlResponse response = null;
			NSError error;
			List<Event> listEvent = new List<Event>();

			NSData dataResponse = NSUrlConnection.SendSynchronousRequest (request,out response,out error);

			if (error == null) {
				NSString stringResponse = NSString.FromData (dataResponse, NSStringEncoding.UTF8);
				var array = JsonArray.Parse (stringResponse);
				foreach(JsonObject obj in array){
					var e = new Event();
					e.ID = obj ["Id"];
					e.Saturday = obj ["Saturday"];
					e.Sunday = obj ["Sunday"];
					e.StartTime = obj ["StartTime"];
					e.EndTime = obj ["EndTime"];
					e.Teaser = obj["Teaser"];
					e.Headline = obj ["Headline"];
					e.Description = obj ["Description"];
					e.Place = obj ["Place"];
					e.Address = obj ["Address"];
					e.Address2 = obj ["Address2"];
					e.Number = obj ["Number"];
					e.Zip = obj ["Zip"];
					e.City = obj ["City"];
					e.Latitude = obj ["Latitude"];
					e.Longitude = obj ["Longitude"];
					e.Thumbnail = obj ["Thumbnail"];
					e.DetailImage = obj ["DetailImage"];
					e.RegistrationRequired = obj ["RegistrationRequired"];
					e.PaymentRequired = obj ["PaymentRequired"];
					// Set or Get Favorite
					e.LookupDatabase ();
					listEvent.Add(e);
				}
			}

			return listEvent;
		}

		public static string GetDayNumberSuffix(DateTime date)
		{
			int day = date.Day;

			switch (day)
			{
				case 1:
				case 21:
				case 31:
				return "st";

				case 2:
				case 22:
				return "nd";

				case 3:
				case 23:
				return "rd";

				default:
				return "th";
			}
		}

		public static DateTime ShortTimeToDate(string shortTime)
		{
			string fullStringDate = string.Format ("{0}/{1}/{2} {3}", DateTime.Now.Year,
			                                       DateTime.Now.Month, 
			                                       DateTime.Now.Day, 
			                                       shortTime);
			if (fullStringDate != null) {
				DateTime dt = Convert.ToDateTime(fullStringDate); 
				return dt;
			}

			return DateTime.Now;
		}

		public static string DescriptionDuringTime(string shortTime1,string shortTime2)
		{
			DateTime d1 = ShortTimeToDate (shortTime1);
			DateTime d2 = ShortTimeToDate (shortTime2);
			string prefixDay = GetDayNumberSuffix (d1);
			string result = string.Format ("{0:dd}{1} of {2:MMMM} at {3}-{4}",d1,prefixDay,d1,d1.Hour,d2.Hour);
			return result;
		}

		public static string DescriptionDuringTime1(string shortTime1,string shortTime2)
		{
			DateTime d1 = ShortTimeToDate (shortTime1);
			DateTime d2 = ShortTimeToDate (shortTime2);
			string result = string.Format ("{0:dd}. {1:MMMM} at {2}-{3}",d1,d1,d1.Hour,d2.Hour);
			return result;
		}

		public static UIImage Crop(UIImage inputCGImage, RectangleF section)
		{	
			/*
			// Start context.
			UIGraphics.BeginImageContext(section.Size);
			var ctx = UIGraphics.GetCurrentContext();

			// Clear the image with red.
			ctx.SetRGBFillColor(255, 255, 255, 255);
			ctx.FillRect(new RectangleF(new PointF(0, 0), section.Size));

			// Setting transform to flip the image.
			var transform = new MonoTouch.CoreGraphics.CGAffineTransform(1, 0, 0, -1, 0, section.Height);
			ctx.ConcatCTM(transform);	

			// Drawing the image.
			//var drawSource;//= {0};//CreateDrawRectangle(image, section);
			ctx.DrawImage(section, inputCGImage.CGImage.WithImageInRect(section));			

			// Extracting the image and ending.
			var croppedImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return croppedImage;
			*/


			var croppedImaged = CIImage.FromCGImage (inputCGImage.CGImage).ImageByCroppingToRect (section);              
			var transformFilter = new CIAffineTransform();
			var affineTransform = CGAffineTransform.MakeTranslation (-150, 150);
			transformFilter.Transform = affineTransform;
			transformFilter.Image = croppedImaged;           
			CIImage transformedImage = transformFilter.OutputImage;

			return UIImage.FromImage (transformedImage);
		}
	}
}

