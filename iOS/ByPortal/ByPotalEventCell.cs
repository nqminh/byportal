#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ByPortal
{
	/// <summary>
	/// By potal event cell.
	/// </summary>
	public partial class ByPotalEventCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("ByPotalEventCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ByPotalEventCell");

		public ByPotalEventCell (IntPtr handle) : base (handle)
		{
		}

		public static ByPotalEventCell Create ()
		{
			NSObject[] objs = Nib.Instantiate (null, null);

			return (ByPotalEventCell)objs [0];
		}

	
		public MonoTouch.UIKit.UILabel Date 
		{ 
			get {
				return (MonoTouch.UIKit.UILabel)this.ViewWithTag (101);
			}
		}

		public MonoTouch.UIKit.UILabel ShortDescription 
		{ 
			get {
				return (MonoTouch.UIKit.UILabel)this.ViewWithTag (103);
			} 
		}

		public ByPotalImageView Photo 
		{ 
			get {
				return (ByPotalImageView)this.ViewWithTag (100);
			} 
		}

		public MonoTouch.UIKit.UILabel Title 
		{ 
			get {
				return (MonoTouch.UIKit.UILabel)this.ViewWithTag (102);
			}  
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews ();
		}
	}
}

