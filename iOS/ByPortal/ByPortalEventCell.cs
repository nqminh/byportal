using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace ByPortal
{
	public partial class ByPortalEventCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("ByPortalEventCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("ByPortalEventCell");

		public ByPortalEventCell (IntPtr handle) : base (handle)
		{
		}

		public static ByPortalEventCell Create ()
		{
			return (ByPortalEventCell)Nib.Instantiate (null, null) [0];
		}
	}
}

