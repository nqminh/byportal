using System;
using MonoTouch.Foundation;
using MonoTouch.FacebookConnect;
using MonoTouch.UIKit;

/// <summary>
/// Facebook utils.
/// </summary>
namespace ByPortal
{
	public class FacebookUtils
	{
		// Properties
		public FBSession Session;

		// Static 
		private static FacebookUtils shared = new FacebookUtils();

		// Delegate
		public delegate void DidFacebookLogin (NSError error);
		public delegate void DidPostStatus(NSError error);

		public static FBSession ActiveSession ()
		{
			return shared.Session;
		}

		public FacebookUtils ()
		{
			string[] permissions = {"email","user_location","user_status","user_birthday","basic_info","publish_stream"};
			this.Session = new FBSession (permissions);
		}

		private void Reset()
		{

			if (this.Session !=null && this.Session.IsOpen) {
				this.Session.CloseAndClearTokenInformation ();
			}
			this.Session = null;
			string[] permissions = {"email","user_location","user_status","user_birthday","basic_info","publish_stream"};
			this.Session = new FBSession (permissions);
		}

		public static bool IsAuthorize()
		{
			if (shared.Session != null) {
				return shared.Session.IsOpen;
			}

			return false;
		}

		public static void ResetSession()
		{
			shared.Reset ();
		}

		void DidFacebookOpenHandle(FBSession session, FBSessionState status, MonoTouch.Foundation.NSError error)
		{

		}

		public static void Login(DidFacebookLogin didLogin)
		{
			if (shared.Session != null) {
				// Check session's state
				if (shared.Session.State == FBSessionState.Created ||
					shared.Session.State == FBSessionState.CreatedTokenLoaded) {
					// Authencication
					//shared.Session.Open (shared.DidFacebookOpenHandle);
				
					shared.Session.Open (FBSessionLoginBehavior.ForcingWebView,
					delegate(FBSession session, FBSessionState status, MonoTouch.Foundation.NSError error) 
					{
						FBSession.ActiveSession = shared.Session;
						// Success
						if (status == FBSessionState.Open) {
							if (didLogin != null) {
								didLogin(null);
							}
						}
						else { // Unsuccess
							if (didLogin != null) {
								didLogin(error);
							}
						}
					});

				}
			}
		}

		public static bool HandleOpenURL(NSUrl url, string sourceApplication)
		{
			return FBAppCall.HandleOpenURL (url, sourceApplication,shared.Session);
		}

		public static void requestGraphAPI(string graphPath,NSDictionary parmaes,string HTTPMethod, FBRequestHandler handler)
		{
			FBRequestConnection requestConn = new FBRequestConnection ();
			FBRequest request = new FBRequest (shared.Session, graphPath, parmaes, HTTPMethod);
			requestConn.AddRequest (request,handler);
			requestConn.Start ();
		}

		public static void PostStatus(Event theEvent, DidPostStatus didPost)
		{
			if (IsAuthorize () && theEvent != null) {
				NSMutableDictionary parames = new NSMutableDictionary ();	
				parames.SetValueForKey (new NSString (theEvent.Teaser), new NSString ("message"));	
				parames.SetValueForKey (new NSString (theEvent.Description), new NSString ("description"));
				parames.SetValueForKey (new NSString (theEvent.Thumbnail), new NSString ("picture"));
				parames.SetValueForKey (new NSString (theEvent.Teaser), new NSString ("caption"));

				requestGraphAPI ("me/feed", parames, @"POST", delegate (FBRequestConnection connection,
				                                                     NSObject result, NSError error) 
				                 {
					if (didPost != null) {
						didPost (error);
					}
				});
			}
			else {
				if (didPost != null) {
					NSError xError = NSError.FromDomain (new NSString("com.byportal.error"), -100);
					didPost(xError);
				}
			}
		}
	}
}

