using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.FacebookConnect;

namespace ByPortal
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;
		ByPortalViewController viewController;
		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			System.Console.WriteLine(ByPotalUtils.DatabaseFilePath);
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			viewController = new ByPortalViewController ();
			UINavigationController navi = new UINavigationController (viewController);

			window.RootViewController = navi;
			window.MakeKeyAndVisible ();
				
			if (options != null) {
				UILocalNotification notification = (UILocalNotification)options.ObjectForKey (UIApplication.LaunchOptionsLocalNotificationKey);

				if (notification != null) {
					NSString alarmMsg = (NSString)notification.UserInfo.ObjectForKey (new NSString ("kByPortalEventAlarm"));
					if (alarmMsg != null) {
						string msg = alarmMsg.ToString ();
						viewController.ShowAlarmMessage (msg);
					}
				}
			}

			return true;
		}

		public override bool OpenUrl (UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{

			return FacebookUtils.HandleOpenURL (url, sourceApplication);
		}

		public override void OnActivated (UIApplication application)
		{
			FBAppCall.HandleDidBecomeActive (FacebookUtils.ActiveSession());
		}

		public override void ReceivedLocalNotification (UIApplication application, UILocalNotification notification)
		{
			NSString alarmMsg = (NSString)notification.UserInfo.ObjectForKey (new NSString("kByPortalEventAlarm"));
			if (alarmMsg != null) {
				string msg = alarmMsg.ToString ();
				viewController.ShowAlarmMessage (msg);
			}
		}
	}
}

