// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace ByPortal
{
	[Register ("ByPortalEventDetailController")]
	partial class ByPortalEventDetailController
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView actionBackground { get; set; }

		[Outlet]
		ByPortal.ByPotalImageView bannerImageView { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnFavorite { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView canvaActions { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView canvaDetails { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView indicator { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblDateTime { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblHeadline { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblLocation { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView viewPopup { get; set; }

		[Action ("alarm:")]
		partial void alarm (MonoTouch.Foundation.NSObject sender);

		[Action ("cancelAlarm:")]
		partial void cancelAlarm (MonoTouch.Foundation.NSObject sender);

		[Action ("checkin:")]
		partial void checkin (MonoTouch.Foundation.NSObject sender);

		[Action ("doneAlarm:")]
		partial void doneAlarm (MonoTouch.Foundation.NSObject sender);

		[Action ("facebook:")]
		partial void facebook (MonoTouch.Foundation.NSObject sender);

		[Action ("favorite:")]
		partial void favorite (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (actionBackground != null) {
				actionBackground.Dispose ();
				actionBackground = null;
			}

			if (bannerImageView != null) {
				bannerImageView.Dispose ();
				bannerImageView = null;
			}

			if (btnFavorite != null) {
				btnFavorite.Dispose ();
				btnFavorite = null;
			}

			if (lblDateTime != null) {
				lblDateTime.Dispose ();
				lblDateTime = null;
			}

			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}

			if (lblHeadline != null) {
				lblHeadline.Dispose ();
				lblHeadline = null;
			}

			if (lblLocation != null) {
				lblLocation.Dispose ();
				lblLocation = null;
			}

			if (viewPopup != null) {
				viewPopup.Dispose ();
				viewPopup = null;
			}

			if (canvaActions != null) {
				canvaActions.Dispose ();
				canvaActions = null;
			}

			if (canvaDetails != null) {
				canvaDetails.Dispose ();
				canvaDetails = null;
			}

			if (indicator != null) {
				indicator.Dispose ();
				indicator = null;
			}
		}
	}
}
