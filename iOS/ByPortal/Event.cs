#if !__IOS__
#error "This file was implemented on iOS"
#endif


using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace ByPortal
{
	public class Event
	{
		public Event ()
		{
		}

		public int 		ID { get; set;}		
		public string 	Address { get; set; }
		public string 	Address2 { get; set; }
		public string 	City { get; set; }
		public string 	Description { get; set; }
		public string 	DetailImage { get; set; }
		public string 	EndTime { get; set; }
		public string 	Headline { get; set; }
		public float 	Latitude { get; set; }
		public float 	Longitude { get; set; }
		public string 	Number { get; set; }
		public bool 	PaymentRequired { get; set; }
		public string 	Place { get; set; }
		public bool 	RegistrationRequired { get; set; }
		public bool 	Saturday { get; set; }
		public string 	StartTime { get; set; }
		public bool 	Sunday { get; set; }
		public string 	Teaser { get; set; }
		public string 	Thumbnail { get; set; }
		public string 	Zip { get; set; }
		public bool 	Favorite { get; set; }
		public NSDate Alarm { get; set; }

		public void LookupDatabase()
		{
			string filedb = ByPotalUtils.DatabaseFilePath;
			NSMutableDictionary dbStored = new NSMutableDictionary (filedb);

			if (dbStored.Count == 0) 
			{
				dbStored = NSMutableDictionary.FromObjectAndKey(NSNumber.FromBoolean(false),
				                                                new NSString(this.ID.ToString()));
				this.Favorite = false;
				dbStored.WriteToFile (filedb,true);
			}
			else 
			{
				NSNumber nfv = (NSNumber)dbStored.ObjectForKey (new NSString (this.ID.ToString ()));

				if (nfv == null) {
					this.Favorite = false;
					dbStored.SetValueForKey (NSNumber.FromBoolean (false), new NSString (this.ID.ToString ()));
					dbStored.WriteToFile (filedb,true);
				} 
				else 
				{
					this.Favorite = nfv.BoolValue;
				}
			}
		}

		public void UpdateFavorite(bool fav)
		{
			try 
			{
				string filedb = ByPotalUtils.DatabaseFilePath;
				NSMutableDictionary dbStored = new NSMutableDictionary (filedb);

				if (dbStored.Count == 0) 
				{
					dbStored = NSMutableDictionary.FromObjectAndKey(NSNumber.FromBoolean(fav),
					                                                new NSString(this.ID.ToString()));
					this.Favorite = fav;
					dbStored.WriteToFile (filedb,true);
				}
				else 
				{
					dbStored.SetValueForKey (NSNumber.FromBoolean(fav),new NSString(this.ID.ToString()));
					this.Favorite = fav;
					dbStored.WriteToFile (filedb,true);
				}
			}
			catch (Exception e) 
			{
				System.Console.WriteLine (e.Message);
			}
		}
	}
}

