#if !__IOS__
#error "This file was implemented on iOS"
#endif


using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Xml;
using MonoTouch.CoreLocation;
using System.Threading;

namespace ByPortal
{
	/// <summary>
	/// By portal view controller.
	/// </summary>
	public partial class ByPortalViewController : UIViewController
	{
		private List<Event> events;
		private List<Event> storedEvents;
		private CLLocationManager locationManager;
		private static UIImage ActiveImage = UIImage.FromBundle("date-item-active.png");
		private static UIImage DeactiveImage = UIImage.FromBundle("date-item-deactive.png");
		public static CLLocation currentLocation;

		public ByPortalViewController () : base ("ByPortalViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear (animated);
			this.tableView.DeselectRow (this.tableView.IndexPathForSelectedRow,true);
		}

		public void InitialEvents()
		{
			this.events = new List<Event> ();

#if HAV_TESTER
			XmlDocument docXML = new XmlDocument ();
			string xmlPath = "/Users/nqminh/Desktop/data-xml.xml";//NSBundle.MainBundle.PathForResource ("data-xml", "xml");
			docXML.Load (xmlPath);

			ByPotalUtils.ListEventRequester ();

			XmlNodeList eventNode = docXML.SelectNodes ("/ArrayOfEvent/Event");

			foreach (XmlNode node in eventNode) 
			{
				Event aEvent = new Event ();

				aEvent.ID = Int32.Parse(node ["Id"].InnerText);
				aEvent.Description = node ["Description"].InnerText;
				aEvent.Teaser = node ["Teaser"].InnerText;
				aEvent.Address = node ["Address"].InnerText;
				aEvent.Address2 = node ["Address2"].InnerText;
				aEvent.City = node ["City"].InnerText;
				aEvent.DetailImage = node ["DetailImage"].InnerText;
				aEvent.Headline = node ["Headline"].InnerText;
				aEvent.Thumbnail = node ["Thumbnail"].InnerText;
				aEvent.StartTime = node ["StartTime"].InnerText;
				aEvent.EndTime = node ["EndTime"].InnerText;
				aEvent.Latitude = float.Parse(node ["Latitude"].InnerText);
				aEvent.Longitude = float.Parse(node ["Longitude"].InnerText);
				aEvent.LookupDatabase ();

				this.events.Add (aEvent);
			}
#endif
			ThreadStart listingEvent = delegate {
				ByPotalUtils.NetworkSpinStart();
				List<Event> les = ByPotalUtils.ListEventRequester();
				ByPotalUtils.NetworkSpinStop();

				this.BeginInvokeOnMainThread (delegate {
					if (les.Count > 0)
						this.toolbarView.Hidden = false;
					this.didListEvent(les);
				});
			};

			Thread t = new Thread (listingEvent);
			t.Start ();
		}

		void didListEvent(List<Event> listEvent)
		{
			this.events = listEvent;
			this.storedEvents = new List<Event> (this.events);
			this.tableView.Source = new ByPotalSource (this.events,this);
			this.tableView.ReloadData ();

		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.InitialEvents ();
			this.Title = @"Find Event";
		
			UIRefreshControl refreshControl = new UIRefreshControl ();
			refreshControl.ValueChanged +=  this.handleRefreshing ;
			this.tableView.AddSubview (refreshControl);

			this.toolbarView.Hidden = true;
			this.toolbarView.Layer.ShadowColor = UIColor.Black.CGColor;
			this.toolbarView.Layer.ShadowOpacity = 0.3f;
			this.toolbarView.Layer.ShadowRadius = 3;
			this.toolbarView.Layer.ShadowOffset = new SizeF (2, 2);

			this.ButtonDate.TouchUpInside += this.ActionDateFilter;
			this.ButtonDistance.TouchUpInside += this.ActionDistabceFilter;

			UIButton favButton = new UIButton (UIButtonType.Custom);
			favButton.Frame = new RectangleF (0, 0, 53, 30);
			favButton.SetBackgroundImage(UIImage.FromBundle("button_favourite_deactive.png"),UIControlState.Normal);
			favButton.SetBackgroundImage(UIImage.FromBundle("button_favourite_active.png"),UIControlState.Selected);
			favButton.TouchUpInside += this.filterFavorite;

			this.NavigationItem.RightBarButtonItem = new UIBarButtonItem (favButton);
			this.NavigationController.NavigationBar.SetBackgroundImage (UIImage.FromBundle("background_top_bar.png"),UIBarMetrics.Default);

			locationManager = new CLLocationManager ();
			locationManager.DesiredAccuracy = 5.0;
			locationManager.Delegate = new LocationDelegate ();
			locationManager.StartUpdatingLocation ();
		}

		void handleRefreshing(object sender, EventArgs e)
		{
			UIRefreshControl refreshControl = (UIRefreshControl)sender;

			// Re-update location
			if (currentLocation == null) {
				locationManager.StopUpdatingLocation ();
				locationManager.StartUpdatingLocation ();
			}

			// Re-load events
			ThreadStart listingEvent = delegate {
				ByPotalUtils.NetworkSpinStart();
				List<Event> les = ByPotalUtils.ListEventRequester();
				ByPotalUtils.NetworkSpinStop();

				this.BeginInvokeOnMainThread (delegate {
					if (les.Count > 0)
						this.toolbarView.Hidden = false;
					this.didListEvent(les);
					refreshControl.EndRefreshing();
				});
			};

			Thread t = new Thread (listingEvent);
			t.Start ();
		}

		void filterFavorite(object sender, EventArgs e)
		{
			UIButton fvButton = (UIButton)sender;
			fvButton.Selected = !fvButton.Selected;

			if (fvButton.Selected) {
				int listCount = this.events.Count;
				for (int ie = 0; ie < listCount; ie++) {

					Event aEvent = this.events [ie];
					if (aEvent.Favorite == false) {
						this.events.Remove (aEvent);
						listCount--;
						ie--;
					}
				}
			} 
			else {
				this.events = new List<Event> (this.storedEvents);
				this.tableView.Source = new ByPotalSource (this.events,this);
			}

			this.tableView.ReloadData ();
		}

		void ActionDateFilter(object sender, EventArgs e)
		{
			this.ButtonDate.SetBackgroundImage (ActiveImage,UIControlState.Normal);
			this.ButtonDistance.SetBackgroundImage (DeactiveImage,UIControlState.Normal);
			this.OnFilterDate ();
		}

		void ActionDistabceFilter(object sender, EventArgs e)
		{
			this.ButtonDate.SetBackgroundImage (DeactiveImage,UIControlState.Normal);
			this.ButtonDistance.SetBackgroundImage (ActiveImage,UIControlState.Normal);
			this.OnFilterDistance ();
		}


		private static int CompareDate(ByPortal.Event e1, ByPortal.Event e2)
		{	
			DateTime d1 = ByPotalUtils.ShortTimeToDate (e1.StartTime);
			DateTime d2 = ByPotalUtils.ShortTimeToDate (e2.StartTime);
			int cp = d1.CompareTo (d2);

			if (cp == 0) {
				DateTime d11 = ByPotalUtils.ShortTimeToDate (e1.EndTime);
				DateTime d21 = ByPotalUtils.ShortTimeToDate (e2.EndTime);
				return d11.CompareTo (d21);
			}

			return d1.CompareTo (d2);
		}

		private static int CompareDistance(ByPortal.Event e1, ByPortal.Event e2)
		{
			CLLocation l1 = new CLLocation (e1.Latitude, e1.Longitude);
			CLLocation l2 = new CLLocation (e2.Latitude, e2.Longitude);

			float d1 = (float)l1.DistanceFrom (ByPortalViewController.currentLocation);
			float d2 = (float)l2.DistanceFrom (ByPortalViewController.currentLocation);

			return (d1 > d2 ? 1 : 0);
		}

		void OnFilterDate()
		{
			this.events.Sort (CompareDate);
			this.tableView.ReloadData ();
		}

		void OnFilterDistance()
		{
			if (currentLocation != null) {
				this.events.Sort (CompareDistance);
				this.tableView.ReloadData ();
			}
			else {
				new UIAlertView ("ByPortal", "Could't locate your position", null, "OK", null).Show();
			}
		}

		public void ShowAlarmMessage(string message)
		{
			new UIAlertView ("Alarm Event", message, null, "OK", null).Show();
		}
	}
	
/// <summary>
/// Location delegate.
/// </summary>
	public class LocationDelegate: CLLocationManagerDelegate
	{
		public LocationDelegate():base()
		{

		}

		public override void LocationsUpdated (CLLocationManager manager, CLLocation[] locations)
		{
			if (locations.Length > 0) {
				ByPortalViewController.currentLocation = locations [locations.Length - 1];
				System.Console.WriteLine ("Current : ({0},{1})",
				                          ByPortalViewController.currentLocation.Coordinate.Latitude,
				                          ByPortalViewController.currentLocation.Coordinate.Longitude);
				manager.StopUpdatingLocation ();
			}
		}

		public override void Failed (CLLocationManager manager, NSError error)
		{
			
		}
	}
}

