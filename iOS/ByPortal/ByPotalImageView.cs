#if !__IOS__
#error "This file was implemented on iOS"
#endif

using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading;

namespace ByPortal
{	
	/// <summary>
	/// By potal image view.
	/// </summary>
	public partial class ByPotalImageView : UIImageView
	{
		public string urlImage { get; set;}
		public bool busy { get; set; }
		public static NSMutableDictionary cached = new NSMutableDictionary ();
		public delegate void DidLoadImage(UIImage image);

		public ByPotalImageView (IntPtr handle) :base(handle) 
		{
		}

		public void setImageURL(string aUrlImage,DidLoadImage loadedImage)
		{
			aUrlImage = aUrlImage.Trim ();
			//if (!aUrlImage.Contains("&width")) aUrlImage += "&width=112";

			UIImage cachedImg = (UIImage)ByPotalImageView.cached.ValueForKey (new NSString(aUrlImage));

			if (cachedImg != null) 
			{
				System.Console.WriteLine ("Already in cached ; {0}",aUrlImage);
				this.Image = cachedImg;
				if (loadedImage != null) 
					loadedImage(cachedImg);
				return;
			}

			if (this.busy) {
				System.Console.WriteLine ("Busying : {0}",aUrlImage);
				return;
			}

			this.urlImage = aUrlImage;
			this.busy = true;

			ThreadStart dowloadingWork = delegate {

				ByPotalUtils.NetworkSpinStart();
				System.Console.WriteLine ("Begin download : {0}",aUrlImage);
				NSUrl url = NSUrl.FromString (this.urlImage);

				if (url == null) {
					ByPotalUtils.NetworkSpinStop();
					if (loadedImage != null) 
						loadedImage(null);
					return;
				}

				NSData imgData = NSData.FromUrl (url);
				ByPotalUtils.NetworkSpinStop();

				if (imgData != null) {
					UIImage imgObj = UIImage.LoadFromData (imgData);

					if (imgObj != null) {
						this.BeginInvokeOnMainThread (delegate {
							this.Image = imgObj;
							if (loadedImage != null) 
								loadedImage(imgObj);
							ByPotalImageView.cached.SetValueForKey(imgObj,new NSString(aUrlImage));

						});
					}
				}

				this.busy = false;
				System.Console.WriteLine ("End download : {0}",aUrlImage);
			};

			Thread t = new Thread (dowloadingWork);
			t.Start ();
		}
	}
}



